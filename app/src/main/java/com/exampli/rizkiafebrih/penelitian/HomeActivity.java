package com.exampli.rizkiafebrih.penelitian;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.kontak_alvin.KontakActivity;
import com.exampli.rizkiafebrih.penelitian.kontak_alvin.NewsActivity;
import com.exampli.rizkiafebrih.penelitian.kontak_alvin.TampilNewsExternal;
import com.exampli.rizkiafebrih.penelitian.main_reminder.MainReminderActivity;
import com.exampli.rizkiafebrih.penelitian.rteditor.RTEditorActivity;
import com.exampli.rizkiafebrih.penelitian.slider.GuideIntroActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth fAuth;
    private DatabaseReference fNotesDatabase;
    private ImageView note, kontak, meeting, news, kalender, surat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        note = (ImageView) findViewById(R.id.note);
        kontak = (ImageView) findViewById(R.id.kontak);
        meeting = (ImageView) findViewById(R.id.jadwal);
        news = (ImageView) findViewById(R.id.berita);
        surat = (ImageView) findViewById(R.id.surat);
        kalender = (ImageView) findViewById(R.id.kalender);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null ){
            Spannable text = new SpannableString(actionBar.getTitle());
            text.setSpan(new ForegroundColorSpan (getResources().getColor(R.color.toolbarTitle)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            actionBar.setTitle(text);
        }

        fAuth = FirebaseAuth.getInstance();
        if (fAuth.getCurrentUser() != null) {
            fNotesDatabase = FirebaseDatabase.getInstance().getReference().child("Notes").child(fAuth.getCurrentUser().getUid());
        }
        note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        kontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, KontakActivity.class);
                startActivity(intent);
            }
        });

        kalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(HomeActivity.this, TampilNewsExternal.class);
                f.putExtra("indikator", "kalender");
                startActivity(f);
                /*Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.google.com/calendar"));
                startActivity(intent);*/
            }
        });

        meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainReminderActivity.class);
                startActivity(intent);
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });

        surat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RTEditorActivity.class);
                startActivity(intent);
            }
        });

        updateUI();
    }

    private void updateUI() {

        if (fAuth.getCurrentUser() != null) {
            Log.i("HomeActivity", "fAuth != null");
        } else {
            Intent startIntent = new Intent(HomeActivity.this, StartActivity.class);
            startActivity(startIntent);
            finish();
            Log.i("HomeActivity", "fAuth == null");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menus, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);


        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.account:
                Intent profileIntent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(profileIntent);
                break;
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(HomeActivity.this, StartActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(HomeActivity.this, "Anda Berhasil Keluar Akun", Toast.LENGTH_SHORT).show();
                break;
            case R.id.help:
                Intent helpIntent = new Intent(HomeActivity.this, GuideIntroActivity.class);
                startActivity(helpIntent);
                break;
        }
        return true;

    }
}
