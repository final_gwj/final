package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.exampli.rizkiafebrih.penelitian.GetTimeAgo;
import com.exampli.rizkiafebrih.penelitian.NewNoteActivity;
import com.exampli.rizkiafebrih.penelitian.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class NoteHistoryListActivity extends AppCompatActivity {

    private RecyclerView mNoteHistoryList;
    private LinearLayoutManager linearLayoutManager;

    private FirebaseAuth fAuth;
    private DatabaseReference mDatabase;

    public String noteHistoryId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_history_list);

        noteHistoryId = getIntent().getExtras().getString("noteHistoryId");

        fAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Notes_History").child(fAuth.getCurrentUser().getUid()).child(noteHistoryId);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            Spannable text = new SpannableString(actionBar.getTitle());
            text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.toolbarTitle)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            actionBar.setTitle(text);
        }

        mNoteHistoryList = (RecyclerView) findViewById(R.id.note_history_list);
        mNoteHistoryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        mNoteHistoryList.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.orderByValue();
        FirebaseRecyclerAdapter<NoteHistoryListItem, NoteHistoryViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<NoteHistoryListItem, NoteHistoryViewHolder>(
                NoteHistoryListItem.class,
                R.layout.item_note_history_list,
                NoteHistoryViewHolder.class,
                query
        ) {
            @Override
            protected void populateViewHolder(NoteHistoryViewHolder viewHolder, NoteHistoryListItem model, int position) {
                final String notePosition = getRef(position).getKey();
                mDatabase.child(notePosition).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                            String title = (String) dataSnapshot.child("title").getValue().toString();
                            String desc = (String) dataSnapshot.child("content").getValue().toString();
                            String timestamp = (String) dataSnapshot.child("timestamp").getValue().toString();

                            viewHolder.setTitle(title);
                            viewHolder.setDescription(desc);

                            GetTimeAgo getTimeAgo = new GetTimeAgo();
                            viewHolder.setNoteTime(getTimeAgo.getTimeAgo(Long.parseLong(timestamp), getApplicationContext()));

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };

        mNoteHistoryList.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
