package com.exampli.rizkiafebrih.penelitian.kontak_alvin;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import com.exampli.rizkiafebrih.penelitian.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExternalFragment extends Fragment {
    GridView gridview;
    AdapterExternal gridviewAdapter;
    ArrayList<ExternalItem> data = new ArrayList<ExternalItem>();


    public ExternalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_external, container, false);

        gridview = (GridView) rootView.findViewById(R.id.gridview);

        data.add(new ExternalItem(getResources().getString(R.string.ekonomi)));
        data.add(new ExternalItem(getResources().getString(R.string.bola)));
        data.add(new ExternalItem(getResources().getString(R.string.teknologi)));
        data.add(new ExternalItem(getResources().getString(R.string.sains)));
        data.add(new ExternalItem(getResources().getString(R.string.entertainment)));
        data.add(new ExternalItem(getResources().getString(R.string.otomotif)));


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent a = new Intent(getActivity(), TampilNewsExternal.class);
                    a.putExtra("indikator", "news");
                    startActivity(a);
                } else if (position == 1) {
                    Intent b = new Intent(getActivity(), TampilNewsExternal.class);
                    b.putExtra("indikator", "bola");
                    startActivity(b);
                } else if (position == 2) {
                    Intent c = new Intent(getActivity(), TampilNewsExternal.class);
                    c.putExtra("indikator", "tekno");
                    startActivity(c);
                } else if (position == 3) {
                    Intent d = new Intent(getActivity(), TampilNewsExternal.class);
                    d.putExtra("indikator", "sains");
                    startActivity(d);
                } else if (position == 4) {
                    Intent e = new Intent(getActivity(), TampilNewsExternal.class);
                    e.putExtra("indikator", "entertainment");
                    startActivity(e);
                } else if (position == 5) {
                    Intent f = new Intent(getActivity(), TampilNewsExternal.class);
                    f.putExtra("indikator", "otomotif");
                    startActivity(f);
                }
            }
        });


        setDataAdapter();
        return rootView;
    }


    private void setDataAdapter() {
        gridviewAdapter = new AdapterExternal(getActivity(), R.layout.item_pilih_news_external, data);
        gridview.setAdapter(gridviewAdapter);
    }
}
