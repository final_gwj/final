package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import com.google.firebase.database.Exclude;

/**
 * Created by Alvin Tandiardi on 03/05/2018.
 */

public class KontakItem {
    private String nama;

    public KontakItem() {
    }

    public KontakItem(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

}
