package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.R;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class KontakActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private Toolbar tb;
    private RecyclerView mKontakList;
    private ProgressDialog mProgress;
    private ProgressBar mProgressBar;

    private DatabaseReference mDatabse;
    private FirebaseAuth fAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontak);

        //inisialisasi toolbar
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);

        mProgressBar = (ProgressBar) findViewById(R.id.kontak_progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);

        //floating action button
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KontakActivity.this, TambahKontakNewActivity.class);
                startActivity(intent);

            }
        });

        fAuth = FirebaseAuth.getInstance();
        if (fAuth.getCurrentUser() != null) {
            mDatabse = FirebaseDatabase.getInstance().getReference().child("Kontak").child(fAuth.getCurrentUser().getUid());
        }

        mKontakList = (RecyclerView) findViewById(R.id.kontak_list);
        mKontakList.setHasFixedSize(true);
        mKontakList.setLayoutManager(new LinearLayoutManager(this));

        mProgress = new ProgressDialog(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<KontakItem, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<KontakItem, BlogViewHolder>(

                KontakItem.class,
                R.layout.item_tampil_kontak,
                BlogViewHolder.class,
                mDatabse.orderByChild("nama")

        ) {
            @Override
            protected void populateViewHolder(final BlogViewHolder viewHolder, KontakItem model, final int position) {

                final String post_key = getRef(position).getKey();

                viewHolder.setTitle(model.getNama());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent detail = new Intent(KontakActivity.this, EditKontakActivity.class);
                        detail.putExtra("kontak_id", post_key);
                        startActivity(detail);
                    }
                });

                viewHolder.mView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                        menu.setHeaderTitle("Choose Your Option");
                        MenuItem delete = menu.add(Menu.NONE, 1, 1, "Delete");

                        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                mProgress.setMessage("Deleting News");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(KontakActivity.this);

                                // set title dialog
                                alertDialogBuilder.setTitle("Delete");

                                // set pesan dari dialog
                                alertDialogBuilder
                                        .setMessage("Are you sure want to delete this item ?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                mProgress.show();
                                                final String selected = getRef(position).getKey();
                                                mDatabse.child(selected).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Toast.makeText(KontakActivity.this, "Contact Deleted", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Log.e("KontakActivity", task.getException().toString());
                                                            Toast.makeText(KontakActivity.this, "ERROR: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                                mProgress.dismiss();
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                // jika tombol ini diklik, akan menutup dialog
                                                // dan tidak terjadi apa2
                                                dialog.cancel();
                                            }
                                        });

                                // membuat alert dialog dari builder
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // menampilkan alert dialog
                                alertDialog.show();

                                return true;
                            }
                        });
                    }
                });
            }

            @Override
            public void onDataChanged() {
                if (mProgressBar != null){
                    mProgressBar.setVisibility(View.GONE);
                }
            }

        };

        mKontakList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class BlogViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public BlogViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

        }

        public void setTitle(String title) {
            TextView post_title = (TextView) mView.findViewById(R.id.namakontak);
            post_title.setText(title);
        }

    }
}