package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.exampli.rizkiafebrih.penelitian.R;

public class TampilNewsExternal extends AppCompatActivity {

    private Toolbar tb;
    private WebView browser;
    private WebSettings webSettings;
    private ProgressBar progressBar;
    private FrameLayout frameLayout;

    private String indikator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_news_external);

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        progressBar = (ProgressBar) findViewById(R.id.news_eksternal_progressbar);

        progressBar.setMax(100);

        browser = (WebView) findViewById(R.id.webview);

        browser.setWebViewClient(new HelpClient());

        browser.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                frameLayout.setVisibility(View.VISIBLE);
                progressBar.setProgress(progress);

                setTitle("Loading. . .");

                if (progress == 100) {
                    frameLayout.setVisibility(View.GONE);
                    setTitle(view.getTitle());
                }
                super.onProgressChanged(view, progress);
            }
        });

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        indikator = getIntent().getExtras().getString("indikator");
        if (indikator.equals("news")) {
            browser.loadUrl("https://ekonomi.kompas.com/");
        } else if (indikator.equals("bola")) {
            tb.setTitle("Bola");
            browser.loadUrl("https://bola.kompas.com/");
        } else if (indikator.equals("tekno")) {
            tb.setTitle("Teknologi");
            browser.loadUrl("https://tekno.kompas.com/");
        } else if (indikator.equals("sains")) {
            tb.setTitle("Sains");
            browser.loadUrl("https://sains.kompas.com/");
        } else if (indikator.equals("entertainment")) {
            tb.setTitle("Entertainment");
            browser.loadUrl("https://entertainment.kompas.com/");
        } else if (indikator.equals("otomotif")) {
            tb.setTitle("Otomotif");
            browser.loadUrl("https://otomotif.kompas.com/");
        } else if (indikator.equals("kalender")) {
            tb.setTitle("Kalender");
            browser.loadUrl("https://www.google.com/calendar");
        }

        webSettings = browser.getSettings();
        webSettings.setJavaScriptEnabled(true); // Untuk mengaktifkan javascript
        webSettings.getUseWideViewPort();
        browser.setVerticalScrollBarEnabled(true);

        progressBar.setProgress(0);

    }

    private class HelpClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            frameLayout.setVisibility(View.VISIBLE);
            return true;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }

    @Override
    public void onBackPressed() {
        // Jika Webview bisa di back maka backward page sebelumnya
        if (browser.canGoBack()) {
            browser.goBack();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
