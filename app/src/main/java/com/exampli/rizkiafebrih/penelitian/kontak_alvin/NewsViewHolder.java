package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.exampli.rizkiafebrih.penelitian.R;

/**
 * Created by Alvin Tandiardi on 01/06/2018.
 */

public class NewsViewHolder extends RecyclerView.ViewHolder {
    View mView;
    TextView newsTitle;
    CardView newsCard;

    public NewsViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        newsTitle = (TextView) mView.findViewById(R.id.judul_news);
        newsCard = (CardView) mView.findViewById(R.id.cardview);
    }

    public void setNewsTitle(String title) {
        newsTitle.setText(title);
    }
}
