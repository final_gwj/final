package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.exampli.rizkiafebrih.penelitian.R;

/**
 * Created by Alvin Tandiardi on 26/07/2018.
 */

public class NoteHistoryViewHolder extends RecyclerView.ViewHolder {

    View mView;
    TextView noteHistoryTitle, noteHistoryDescription, noteHistoryTIme;

    public NoteHistoryViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        noteHistoryTitle = (TextView) mView.findViewById(R.id.note_history_title);
        noteHistoryDescription = (TextView) mView.findViewById(R.id.note_history_desc);
        noteHistoryTIme = (TextView) mView.findViewById(R.id.note_history_time);

    }

    public void setTitle(String title) {
        noteHistoryTitle.setText(title);
    }

    public void setDescription(String description) {
        noteHistoryDescription.setText(description);
    }

    public void setNoteTime(String time) {
        noteHistoryTIme.setText(time);
    }

}
