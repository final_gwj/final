package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

/**
 * Created by Alvin Tandiardi on 26/07/2018.
 */

public class NoteHistoryListItem {

    private String noteTitle;
    private String noteDescription;
    private String noteTime;

    public NoteHistoryListItem() {
    }

    public NoteHistoryListItem(String noteTitle, String noteDescription, String noteTime) {
        this.noteTitle = noteTitle;
        this.noteDescription = noteDescription;
        this.noteTime = noteTime;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public void setNoteDescription(String noteDescription) {
        this.noteDescription = noteDescription;
    }

    public String getNoteTime() {
        return noteTime;
    }

    public void setNoteTime(String noteTime) {
        this.noteTime = noteTime;
    }
}
