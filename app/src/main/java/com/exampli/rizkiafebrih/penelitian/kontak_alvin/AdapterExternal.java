package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.exampli.rizkiafebrih.penelitian.R;

import java.util.ArrayList;


public class AdapterExternal extends ArrayAdapter<ExternalItem> {
    Context mContext;
    int resourceId;
    ArrayList<ExternalItem> data = new ArrayList<ExternalItem>();

    public AdapterExternal (Context context, int layoutResourceId, ArrayList<ExternalItem> data) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.resourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        AdapterExternal.ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new AdapterExternal.ViewHolder();
            holder.txtjudul = (TextView) itemView.findViewById(R.id.judul);
            itemView.setTag(holder);
        } else {
            holder = (AdapterExternal.ViewHolder) itemView.getTag();
        }

        ExternalItem item = getItem(position);
        holder.txtjudul.setText(item.getJudul());

        return itemView;
    }

    static class ViewHolder {
        TextView txtjudul;
    }
}
