package com.exampli.rizkiafebrih.penelitian.kontak_alvin;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.GetTimeAgo;
import com.exampli.rizkiafebrih.penelitian.GridSpacingItemDecoration;
import com.exampli.rizkiafebrih.penelitian.MainActivity;
import com.exampli.rizkiafebrih.penelitian.NewNoteActivity;
import com.exampli.rizkiafebrih.penelitian.NoteModel;
import com.exampli.rizkiafebrih.penelitian.NoteViewHolder;
import com.exampli.rizkiafebrih.penelitian.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.pdf.parser.Line;

import static com.onegravity.rteditor.api.RTApi.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteNowFragment extends Fragment {

    private FirebaseAuth fAuth;
    private DatabaseReference fNotesDatabase;
    private DatabaseReference fNotesHistoryDatabase;

    private RecyclerView mNotesList;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private ProgressDialog mProgress;
    private ProgressBar mProgressBar;

    private GetTimeAgo getTimeAgo;

    public NoteNowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_note_now, container, false);

        fAuth = FirebaseAuth.getInstance();
        if (fAuth.getCurrentUser() != null) {
            fNotesDatabase = FirebaseDatabase.getInstance().getReference().child("Notes").child(fAuth.getCurrentUser().getUid());
            fNotesHistoryDatabase = FirebaseDatabase.getInstance().getReference().child("Notes_History").child(fAuth.getCurrentUser().getUid());
        }

        mNotesList = (RecyclerView) view.findViewById(R.id.notes_list);
//        gridLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        mNotesList.setHasFixedSize(true);
        mNotesList.setLayoutManager(linearLayoutManager);
//        mNotesList.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));

        mProgress = new ProgressDialog(getActivity());
        mProgressBar = (ProgressBar) view.findViewById(R.id.note_progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);
        getTimeAgo = new GetTimeAgo();
        loadData();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void loadData() {
        Query query = fNotesDatabase.orderByValue();
        FirebaseRecyclerAdapter<NoteModel, NoteViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<NoteModel, NoteViewHolder>(
                NoteModel.class,
                R.layout.single_note_layout,
                NoteViewHolder.class,
                query

        ) {
            @Override
            protected void populateViewHolder(final NoteViewHolder viewHolder, NoteModel model, int position) {
                DatabaseReference ref = getRef(position);
                mProgressBar.setVisibility(View.GONE);
                final String noteId = getRef(position).getKey();

                fNotesDatabase.child(noteId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild("title") && dataSnapshot.hasChild("timestamp")) {
                            String title = dataSnapshot.child("title").getValue().toString();
                            String description = dataSnapshot.child("content").getValue().toString();
                            String timestamp = dataSnapshot.child("timestamp").getValue().toString();

                            viewHolder.setNoteTitle(title);
                            //viewHolder.setNoteTime(timestamp);
                            viewHolder.setNoteDescription(description);

                            viewHolder.setNoteTime(getTimeAgo.getTimeAgo(Long.parseLong(timestamp), getContext()));

                            viewHolder.noteCard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(getActivity(), NewNoteActivity.class);
                                    intent.putExtra("noteId", noteId);
                                    startActivity(intent);
                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                });

                viewHolder.noteCard.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                        menu.setHeaderTitle("Choose Your Option");
                        MenuItem delete = menu.add(Menu.NONE, 1, 1, "Delete");

                        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                mProgress.setMessage("Deleting News");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                                // set title dialog
                                alertDialogBuilder.setTitle("Delete");

                                // set pesan dari dialog
                                alertDialogBuilder
                                        .setMessage("Are you sure want to delete this item ?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                mProgress.show();
                                                String selected = ref.getKey();

                                                fNotesHistoryDatabase.child(noteId).removeValue();
                                                fNotesDatabase.child(noteId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Toast.makeText(getActivity(), "Note Deleted", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Log.e("MainActivity", task.getException().toString());
                                                            Toast.makeText(getActivity(), "ERROR : " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                                mProgress.dismiss();
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                // jika tombol ini diklik, akan menutup dialog
                                                // dan tidak terjadi apa2
                                                dialog.cancel();
                                            }
                                        });

                                // membuat alert dialog dari builder
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // menampilkan alert dialog
                                alertDialog.show();
                                return true;
                            }
                        });
                    }
                });

            }

            @Override
            public void onDataChanged() {
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }

        };
        mNotesList.setAdapter(firebaseRecyclerAdapter);
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
