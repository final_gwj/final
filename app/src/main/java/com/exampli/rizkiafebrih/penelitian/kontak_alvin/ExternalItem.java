package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

/**
 * Created by Alvin Tandiardi on 04/05/2018.
 */

public class ExternalItem {

    String judul;

    public ExternalItem(String judul) {
        this.judul = judul;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }
}
