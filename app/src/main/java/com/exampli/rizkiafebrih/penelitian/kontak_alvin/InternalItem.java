package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.graphics.drawable.Drawable;

/**
 * Created by Alvin Tandiardi on 03/05/2018.
 */

public class InternalItem {

    String judul;

    public InternalItem() {
    }

    public InternalItem(String judul) {
        this.judul = judul;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

}
