package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class SingleNewsView extends AppCompatActivity {

    private Toolbar tb;
    private TextView title, desc;
    private ImageView newsimage;

    private String image_default = "default";
    String mPostKey = null;

    private ProgressBar mProgressBar;

    private DatabaseReference mDatabase;
    private FirebaseAuth fAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_news_view);

        mPostKey = getIntent().getExtras().getString("NewsId");

        //inisialisasi toolbar
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        //tombol back toolbar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        fAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("News").child(fAuth.getCurrentUser().getUid());

        title = (TextView) findViewById(R.id.news_title);
        desc = (TextView) findViewById(R.id.news_desc);
        newsimage = (ImageView) findViewById(R.id.news_image);

        newsimage.setImageResource(R.drawable.noimage);

        mProgressBar = (ProgressBar) findViewById(R.id.news_progress_bar_load);

        getData(mPostKey);
    }

    private void getData(String key) {
        mDatabase.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_title = (String) dataSnapshot.child("title").getValue();
                String post_desc = (String) dataSnapshot.child("desc").getValue();
                String post_image = (String) dataSnapshot.child("image").getValue();

                title.setText(post_title);
                desc.setText(post_desc);

                if (post_image != "default") {

                    Picasso.get()
                            .load(post_image)
                            .error(R.drawable.no_image1)
                            .into(newsimage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    mProgressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    mProgressBar.setVisibility(View.GONE);
                                }
                            });


//                    Glide.with(getApplicationContext())
//                            .load(post_image)
//                            .error(R.drawable.no_image1)
//                            .crossFade()
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    mProgressBar.setVisibility(View.GONE);
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    mProgressBar.setVisibility(View.GONE);
//                                    return false;
//                                }
//                            })
//                            .into(newsimage);
                }

                if (post_image == "default") {
//                    Glide.with(getApplicationContext())
//                            .load(post_image)
//                            .fallback(R.drawable.no_image1)
//                            .error(R.drawable.no_image1)
//                            .into(newsimage);
                    Picasso.get()
                            .load(post_image)
                            .error(R.drawable.no_image1)
                            .into(newsimage);
                    mProgressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
