package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.exampli.rizkiafebrih.penelitian.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SIngleKontakView extends AppCompatActivity {

    private TextView nama, perusahaan, jabatan, alamat, nomor, email;
    private Toolbar tb;

    private DatabaseReference mDatabase;
    private FirebaseAuth fAuth;
    String mPost_key = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_kontak_view);

        mPost_key = getIntent().getExtras().getString("kontak_id");

        nama = (TextView) findViewById(R.id.namakontak);
        perusahaan = (TextView) findViewById(R.id.perusahaankontak);
        jabatan = (TextView) findViewById(R.id.jabatankontak);
        alamat = (TextView) findViewById(R.id.alamatkontak);
        nomor = (TextView) findViewById(R.id.teleponkontak);
        email = (TextView) findViewById(R.id.emailkontak);

        //inisialisasi toolbar
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        //tombol back toolbar
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        fAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Kontak").child(fAuth.getCurrentUser().getUid());

        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_nama = (String) dataSnapshot.child("nama").getValue();
                String post_perusahaan = (String) dataSnapshot.child("perusahaan").getValue();
                String post_jabatan = (String) dataSnapshot.child("jabatan").getValue();
                String post_alamat = (String) dataSnapshot.child("alamat").getValue();
                String post_nomor = (String) dataSnapshot.child("nomor").getValue();
                String post_email = (String) dataSnapshot.child("email").getValue();

                nama.setText(post_nama);
                perusahaan.setText(post_perusahaan);
                jabatan.setText(post_jabatan);
                alamat.setText(post_alamat);
                nomor.setText(post_nomor);
                email.setText(post_email);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
