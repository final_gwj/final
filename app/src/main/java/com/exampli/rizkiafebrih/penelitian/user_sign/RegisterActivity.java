package com.exampli.rizkiafebrih.penelitian.user_sign;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.HomeActivity;
import com.exampli.rizkiafebrih.penelitian.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity {

    private CircleImageView mProfileImage;
    private Button btnReg;
    private TextInputLayout inName, inEmail, inPass, inNomor, inAlamat;
    private RelativeLayout registerImageContainer;

    private FirebaseAuth fAuth;
    private DatabaseReference fUsersDatabase;
    private StorageReference mStorage;

    private Uri mImageUri = null;
    private static final int GALLERY_REQUEST = 1;

    private ProgressDialog progressDialog;

    //Permision code that will be checked in the method onRequestPermissionsResult
    private int STORAGE_PERMISSION_CODE = 23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnReg = (Button) findViewById(R.id.btn_reg);
        inName = (TextInputLayout) findViewById(R.id.input_reg_name);
        inEmail = (TextInputLayout) findViewById(R.id.input_reg_email);
        inPass = (TextInputLayout) findViewById(R.id.input_reg_pass);
        inAlamat = (TextInputLayout) findViewById(R.id.input_reg_alamat);
        inNomor = (TextInputLayout) findViewById(R.id.input_reg_nomor);
        mProfileImage = (CircleImageView) findViewById(R.id.profile_image);
        registerImageContainer = (RelativeLayout) findViewById(R.id.register_image_container);

        registerImageContainer.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        fAuth = FirebaseAuth.getInstance();
        fUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mStorage = FirebaseStorage.getInstance().getReference();

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            Spannable text = new SpannableString(actionBar.getTitle());
            text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.toolbarTitle)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            actionBar.setTitle(text);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //First checking if the app is already having the permission
                if (isReadStorageAllowed()) {
                    //If permission is already having then showing the toast
                    Toast.makeText(RegisterActivity.this, "You already have the permission", Toast.LENGTH_LONG).show();
                    //Existing the method with return
                    Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, GALLERY_REQUEST);
                    return;
                }
                //If the app has not the permission then asking for the permission
                requestStoragePermission();
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uname = inName.getEditText().getText().toString().trim();
                String uemail = inEmail.getEditText().getText().toString().trim();
                String upass = inPass.getEditText().getText().toString().trim();
                String unomor = inNomor.getEditText().getText().toString().trim();
                String ualamat = inAlamat.getEditText().getText().toString().trim();

                registerUser(uname, uemail, upass, unomor, ualamat);

            }
        });

    }

    private void registerUser(final String name, String email, String password, String nomor, String alamat) {

        if (TextUtils.isEmpty(name)) {
            inName.setError("Nama harus diisi !");
        }

        if (TextUtils.isEmpty(email)) {
            inEmail.setError("Email harus diisi !");
        }

        if (TextUtils.isEmpty(password)) {
            inPass.setError("Password harus diisi !");
        }

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Processing your request, please wait...");

            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);

            fAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {

                                if (mImageUri == null) {
                                    DatabaseReference newPost = fUsersDatabase.child(fAuth.getCurrentUser().getUid());

                                    newPost.child("nama").setValue(name);
                                    newPost.child("nomor").setValue(nomor);
                                    newPost.child("alamat").setValue(alamat);
                                    newPost.child("email").setValue(email);
//                                    newPost.child("password").setValue(password);
                                    newPost.child("image").setValue("default");
                                } else {
                                    StorageReference filepath = mStorage.child("Profile_Photo").child(mImageUri.getLastPathSegment());

                                    filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                            Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                            DatabaseReference newPost = fUsersDatabase.child(fAuth.getCurrentUser().getUid());

                                            newPost.child("nama").setValue(name);
                                            newPost.child("nomor").setValue(nomor);
                                            newPost.child("alamat").setValue(alamat);
                                            newPost.child("email").setValue(email);
//                                            newPost.child("password").setValue(password);
                                            newPost.child("image").setValue(downloadUrl.toString());

                                        }
                                    });
                                }

                                progressDialog.dismiss();

                                Intent mainIntent = new Intent(RegisterActivity.this, HomeActivity.class);
                                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);
                                Toast.makeText(RegisterActivity.this, "User created!", Toast.LENGTH_SHORT).show();

                            } else {

                                progressDialog.dismiss();

                                Toast.makeText(RegisterActivity.this, "ERROR: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    //untuk mendapat data image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setFixAspectRatio(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(this);

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                mImageUri = result.getUri();

                mProfileImage.setImageURI(mImageUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    //kodingan untuk meminta permission khusus marshmallow ke atas
    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }

    }

}
