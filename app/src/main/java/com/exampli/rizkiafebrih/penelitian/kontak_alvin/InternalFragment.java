package com.exampli.rizkiafebrih.penelitian.kontak_alvin;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.GetTimeAgo;
import com.exampli.rizkiafebrih.penelitian.MainActivity;
import com.exampli.rizkiafebrih.penelitian.NewNoteActivity;
import com.exampli.rizkiafebrih.penelitian.NoteModel;
import com.exampli.rizkiafebrih.penelitian.NoteViewHolder;
import com.exampli.rizkiafebrih.penelitian.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


/**
 * A simple {@link Fragment} subclass.
 */
public class InternalFragment extends Fragment {

    private FloatingActionButton fab;
    private RecyclerView mNewsList;
    private LinearLayoutManager linearLayoutManager;

    private DatabaseReference mDatabase;
    private FirebaseStorage mStorage;
    private FirebaseAuth fAuth;

    private String imageDefault = "default";
    private String mImageUrl = null;

    private ProgressDialog mProgress;
    private ProgressBar mProgressBar;
    String image = "default";

    public InternalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_internal, container, false);

        fAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();
        if (fAuth.getCurrentUser() != null) {
            mDatabase = FirebaseDatabase.getInstance().getReference().child("News").child(fAuth.getCurrentUser().getUid());
        }

        //floating action button
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TambahNewsInternal.class);
                startActivity(intent);

            }
        });

        mNewsList = (RecyclerView) rootView.findViewById(R.id.berita_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        mNewsList.setHasFixedSize(true);
        mNewsList.setLayoutManager(linearLayoutManager);

        mProgress = new ProgressDialog(getActivity());

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.news_progress_bar);

        showProgressBar();

        loadData();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void loadData() {
        FirebaseRecyclerAdapter<InternalItem, NewsViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<InternalItem, NewsViewHolder>(

                InternalItem.class,
                R.layout.item_pilih_news,
                NewsViewHolder.class,
                mDatabase.orderByPriority()

        ) {

            @Override
            protected void populateViewHolder(final NewsViewHolder viewHolder, InternalItem model, int position) {
                DatabaseReference ref = getRef(position);
                final String NewsId = getRef(position).getKey();
                final DatabaseReference ImageUrl = getRef(position).child("image");

                ValueEventListener retrieveData = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChild("title")) {
                            String title = (String) dataSnapshot.child("title").getValue().toString();

                            image = (String) dataSnapshot.child("image").getValue();
                            closeProgressBar();
                            viewHolder.setNewsTitle(title);

                            viewHolder.newsCard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Intent intent = new Intent(getContext(), SingleNewsView.class);
                                    intent.putExtra("NewsId", NewsId);
                                    startActivity(intent);
                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        closeProgressBar();
                    }

                };

                mDatabase.child(NewsId).addValueEventListener(retrieveData);
                closeProgressBar();

                viewHolder.newsCard.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                        menu.setHeaderTitle("Choose Your Option");
                        MenuItem delete = menu.add(Menu.NONE, 1, 1, "Delete");

                        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                mProgress.setMessage("Deleting");

                                String selected = ref.getKey();

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                                // set title dialog
                                alertDialogBuilder.setTitle("Delete");

                                // set pesan dari dialog
                                alertDialogBuilder
                                        .setMessage("Are you sure want to delete this item ?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                mProgress.show();
                                                mProgress.setCanceledOnTouchOutside(false);
                                                //menghapus gambar di storage
                                                deleteImage(selected);

                                                //menghapus dari database news
                                                mDatabase.child(selected).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Toast.makeText(getActivity(), "News Deleted", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Log.e("KontakActivity", task.getException().toString());
                                                            Toast.makeText(getActivity(), "ERROR: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                                mProgress.dismiss();

                                            }

                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                // jika tombol ini diklik, akan menutup dialog
                                                // dan tidak terjadi apa2
                                                dialog.cancel();
                                            }
                                        });

                                // membuat alert dialog dari builder
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // menampilkan alert dialog
                                alertDialog.show();

                                return true;
                            }

                        });
                    }
                });

            }

            @Override
            public void onDataChanged() {
                if (mProgressBar != null) {
                    closeProgressBar();
                }
            }
        };

        mNewsList.setAdapter(firebaseRecyclerAdapter);
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void closeProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void deleteImage(String newsId) {

        mDatabase.child(newsId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String imageUrl = (String) dataSnapshot.child("image").getValue();
                try {

                    if (imageUrl.equals("default")) {
                        Toast.makeText(getActivity(), "No Image", Toast.LENGTH_SHORT).show();
                    } else {
                        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);

                        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getActivity(), "Image deleted", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}

