package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.exampli.rizkiafebrih.penelitian.R;

public class NewsActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tl;
    Toolbar tb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tl = (TabLayout) findViewById(R.id.tablayout);
        MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tl.setupWithViewPager(viewPager);
    }

    class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f = null;
            if (position == 0) {
                f = new InternalFragment();
            }
            if (position == 1) {
                f = new ExternalFragment();
            }
            return f;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String name = null;
            if (position == 0) {
                name = "Internal";
            }
            if (position == 1) {
                name = "External";
            }
            return name;
        }
    }
}
