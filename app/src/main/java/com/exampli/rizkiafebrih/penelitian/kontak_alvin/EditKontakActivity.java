package com.exampli.rizkiafebrih.penelitian.kontak_alvin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.exampli.rizkiafebrih.penelitian.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class EditKontakActivity extends AppCompatActivity {
    private Toolbar tb;
    private EditText mPostnama, mPostperusahaan, mPostjabatan, mPostalamat, mPostnomor, mPostemail;
    private ProgressDialog mProgress;

    private String Kontak;
    private boolean isExist;

    private FirebaseAuth fAuth;
    private DatabaseReference mDatabase;
    String mPost_key = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kontak);

        mPostnama = (EditText) findViewById(R.id.inputnama);
        mPostperusahaan = (EditText) findViewById(R.id.inputperusahaan);
        mPostjabatan = (EditText) findViewById(R.id.inputjabatan);
        mPostalamat = (EditText) findViewById(R.id.inputalamat);
        mPostnomor = (EditText) findViewById(R.id.inputnomor);
        mPostemail = (EditText) findViewById(R.id.inputemail);
        mProgress = new ProgressDialog(this);

        //inisialisasi toolbar
        tb = (Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tb);
        //tombol back toolbar
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //firebase
        fAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Kontak").child(fAuth.getCurrentUser().getUid());


        try {
            mPost_key = getIntent().getStringExtra("kontak_id");

            //Toast.makeText(this, noteID, Toast.LENGTH_SHORT).show();

            if (!mPost_key.trim().equals("")) {
                isExist = true;
            } else {
                isExist = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //hide keyboard in edit text when open activity
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //menampilkan data kontak
        putData();
    }


    //buat tanda save
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //ketika save di klik
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                break;
            case R.id.save:
                startPosting();
                break;
        }
        return true;
    }

    private void startPosting() {
        if (fAuth.getCurrentUser() != null) {

            if (isExist) {

                if (TextUtils.isEmpty(mPostnama.getText())) {

                    mPostnama.setError("Name is required !");

                }
                if (TextUtils.isEmpty(mPostnomor.getText())) {

                    mPostnomor.setError("Number is required !");

                }
                if (!TextUtils.isEmpty(mPostnama.getText()) && !TextUtils.isEmpty(mPostnomor.getText())) {
                    // UPDATE A NOTE
                    Map updateMap = new HashMap();
                    updateMap.put("nama", mPostnama.getText().toString().trim());
                    updateMap.put("perusahaan", mPostperusahaan.getText().toString().trim());
                    updateMap.put("jabatan", mPostjabatan.getText().toString().trim());
                    updateMap.put("alamat", mPostalamat.getText().toString().trim());
                    updateMap.put("nomor", mPostnomor.getText().toString().trim());
                    updateMap.put("email", mPostemail.getText().toString().trim());

                    mDatabase.child(mPost_key).updateChildren(updateMap);

                    Toast.makeText(this, "Kontak di Perbaharui", Toast.LENGTH_SHORT).show();

                    finish();
                }

            }
//            } else {
//                mProgress = new ProgressDialog(EditKontakActivity.this);
//                mProgress.setMessage("Menyimpan Kontak . . .");
//                mProgress.show();
//
//                final String nama = mPostnama.getText().toString().trim();
//                final String perusahaan = mPostperusahaan.getText().toString().trim();
//                final String jabatan = mPostjabatan.getText().toString().trim();
//                final String alamat = mPostalamat.getText().toString().trim();
//                final String nomor = mPostnomor.getText().toString().trim();
//                final String email = mPostemail.getText().toString().trim();
//
//                DatabaseReference newPost = mDatabase.push();
//                newPost.child("nama").setValue(nama);
//                newPost.child("perusahaan").setValue(perusahaan);
//                newPost.child("jabatan").setValue(jabatan);
//                newPost.child("alamat").setValue(alamat);
//                newPost.child("nomor").setValue(nomor);
//                newPost.child("email").setValue(email);
//
//                mProgress.dismiss();
//                startActivity(new Intent(EditKontakActivity.this, KontakActivity.class));
//            }
        }
    }

    private void putData() {

        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_nama = (String) dataSnapshot.child("nama").getValue();
                String post_perusahaan = (String) dataSnapshot.child("perusahaan").getValue();
                String post_jabatan = (String) dataSnapshot.child("jabatan").getValue();
                String post_alamat = (String) dataSnapshot.child("alamat").getValue();
                String post_nomor = (String) dataSnapshot.child("nomor").getValue();
                String post_email = (String) dataSnapshot.child("email").getValue();

                mPostnama.setText(post_nama);
                mPostperusahaan.setText(post_perusahaan);
                mPostjabatan.setText(post_jabatan);
                mPostalamat.setText(post_alamat);
                mPostnomor.setText(post_nomor);
                mPostemail.setText(post_email);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
