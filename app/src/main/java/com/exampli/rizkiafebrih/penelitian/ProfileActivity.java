package com.exampli.rizkiafebrih.penelitian;

import android.*;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    //    private CircleImageView mImageProfile;
    private ImageView mImageProfile;
    private EditText profileName, profileNomor, profileAlamat;
    private TextView profileEmail;
    private ProgressDialog progressDialog;
    private LinearLayout circleImageContainer;

    private Uri mImageUri = null;
    private static final int GALLERY_REQUEST = 1;
    private String profile_image = null;
    //Permision code that will be checked in the method onRequestPermissionsResult
    private int STORAGE_PERMISSION_CODE = 23;

    private FirebaseAuth fAuth;
    private StorageReference mStorage;
    private FirebaseStorage mFirebaseStorage;
    private DatabaseReference mDatabase;

    private static final String TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //inisialisasi
        mImageProfile = (ImageView) findViewById(R.id.profile_image);
        profileName = (EditText) findViewById(R.id.profile_name);
        profileNomor = (EditText) findViewById(R.id.profile_nomor);
        profileAlamat = (EditText) findViewById(R.id.profile_alamat);
        profileEmail = (TextView) findViewById(R.id.profile_email);
        circleImageContainer = (LinearLayout) findViewById(R.id.circle_image_container);
        circleImageContainer.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");

        mImageProfile.setOnClickListener(this);

        fAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(fAuth.getCurrentUser().getUid());
        mStorage = FirebaseStorage.getInstance().getReference();
        mFirebaseStorage = FirebaseStorage.getInstance();

        //hide keyboard in edit text when open activity
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            Spannable text = new SpannableString(actionBar.getTitle());
            text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.toolbarTitle)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            actionBar.setTitle(text);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);

        putData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image:
                //First checking if the app is already having the permission
                if (isReadStorageAllowed()) {
                    //If permission is already having then showing the toast
                    Toast.makeText(ProfileActivity.this, "You already have the permission", Toast.LENGTH_LONG).show();
                    //Existing the method with return
                    Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, GALLERY_REQUEST);
                    return;
                }
                //If the app has not the permission then asking for the permission
                requestStoragePermission();
                break;
        }
    }

    private void startPosting() {
        if (fAuth.getCurrentUser() != null) {

            if (!TextUtils.isEmpty(profileName.getText()) && !TextUtils.isEmpty(profileNomor.getText())) {
                // UPDATE A PROFILE
                Map updateMap = new HashMap();
                updateMap.put("nama", profileName.getText().toString().trim());
                updateMap.put("nomor", profileNomor.getText().toString().trim());
                updateMap.put("alamat", profileAlamat.getText().toString().trim());

                if (mImageUri != null) {
                    if (profile_image.equals("default")) {
                        //hapus image di database
                        mDatabase.child("image").removeValue();

                        StorageReference filepath = mStorage.child("Profile_Photo").child(mImageUri.getLastPathSegment());
                        //upload ke storage dan masukin ke database
                        filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                DatabaseReference newPost = mDatabase;

                                newPost.child("image").setValue(downloadUrl.toString());
                            }
                        });
                    } else {
                        StorageReference imageRef = mFirebaseStorage.getReferenceFromUrl(profile_image);
                        //hapus image di storage
                        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ProfileActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        });

                        //hapus image di database
                        mDatabase.child("image").removeValue();

                        StorageReference filepath = mStorage.child("Profile_Photo").child(mImageUri.getLastPathSegment());
                        //upload ke storage dan masukin ke database
                        filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                DatabaseReference newPost = mDatabase;

                                newPost.child("image").setValue(downloadUrl.toString());
                            }
                        });
                    }

                }

                mDatabase.updateChildren(updateMap);

                Toast.makeText(this, "Profile di Perbaharui", Toast.LENGTH_SHORT).show();

                finish();
            }
        }

    }

    private void putData() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String profile_nama = (String) dataSnapshot.child("nama").getValue();
                String profile_nomor = (String) dataSnapshot.child("nomor").getValue();
                String profile_alamat = (String) dataSnapshot.child("alamat").getValue();
                String profile_email = (String) dataSnapshot.child("email").getValue();
                profile_image = (String) dataSnapshot.child("image").getValue();

                Log.d(TAG, "value profile image " + profile_image);

                profileName.setText(profile_nama);
                profileNomor.setText(profile_nomor);
                profileAlamat.setText(profile_alamat);
                profileEmail.setText(profile_email);
//                Glide.with(getApplicationContext())
//                        .load(profile_image)
//                        .placeholder(R.drawable.ic_account)
//                        .error(R.drawable.ic_account)
//                        .fallback(R.drawable.ic_account)
//                        .dontAnimate()
//                        .into(mImageProfile);
                if (profile_image != "default") {
                    Picasso.get()
                            .load(profile_image)
                            .placeholder(R.drawable.ic_account)
                            .error(R.drawable.ic_account)
                            .noFade()
                            .into(mImageProfile, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    progressDialog.dismiss();
                                }

                                @Override
                                public void onError(Exception e) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ProfileActivity.this, "Image not found", Toast.LENGTH_SHORT).show();
                                }
                            });
                }

                if (profile_image == "default") {
                    Picasso.get()
                            .load(profile_image)
                            .error(R.drawable.ic_account)
                            .noFade()
                            .into(mImageProfile);
                    progressDialog.dismiss();
                    Toast.makeText(ProfileActivity.this, "Image not found", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.save:
                startPosting();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    //untuk mendapat data image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setFixAspectRatio(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(this);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                mImageUri = result.getUri();

                mImageProfile.setImageURI(mImageUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    //kodingan untuk meminta permission khusus marshmallow ke atas
    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }

    }

}
